## React/redux bookmark management app

This project was created with [Create React App]

This is the app for storing the bookmarks, it allows you store the URLs that you interested in with some additional information (title, tags by theme) and follow them

## Functionality

* CRUD-operation
* Items searching (filtration by "tags")
* Change color mode of the app

## Screenshot of the app

### The whole app page

![image](https://user-images.githubusercontent.com/95375495/144459944-1df6a6ea-aa10-4994-bcba-77245577a5dd.png)

### Bookmark creating

![image](https://user-images.githubusercontent.com/95375495/144460471-b97e1ccd-c9ac-4973-8cae-d3d9e3a202de.png)

### Form validation

![image](https://user-images.githubusercontent.com/95375495/144460794-160ee6fa-6b10-4b7d-866e-7916cf4c2d0a.png)

### Using self-made "tag-input" with searching through the all existing tags

![image](https://user-images.githubusercontent.com/95375495/144461651-eee30335-bb9f-4016-8e3f-3ce8bc184f62.png)


![image](https://user-images.githubusercontent.com/95375495/144461787-483ec53f-f9dd-432c-99fb-af6091eb56b6.png)


### Bookmark updating 

![image](https://user-images.githubusercontent.com/95375495/144462098-a7356955-5bc7-4adc-ac74-04f3d5f96e11.png)


![image](https://user-images.githubusercontent.com/95375495/144462374-2f01829f-2a31-4863-b9bb-0bd35ad0a454.png)


![image](https://user-images.githubusercontent.com/95375495/144462605-1d570209-80ac-4c9d-abbe-777aeb053e55.png)


![image](https://user-images.githubusercontent.com/95375495/144462836-71796020-31a8-4a2b-ad34-bf81d94fc1e8.png)


### Searching bookmarks (filtering all bookmarks items by tags they consist of)

![image](https://user-images.githubusercontent.com/95375495/144469252-0fb8525e-ca35-49c2-bd54-edde1fddcea1.png)


![image](https://user-images.githubusercontent.com/95375495/144469502-84e6f471-29b5-4ce1-9845-2c8a1d4c07d2.png)


![image](https://user-images.githubusercontent.com/95375495/144470285-adde460c-4a76-452e-bb91-899d87549954.png)



### Changing color mode

![image](https://user-images.githubusercontent.com/95375495/144471730-7ffe948b-ace0-4979-af35-df74da55a381.png)


![image](https://user-images.githubusercontent.com/95375495/144471807-cb9303ae-ffce-4b7f-be73-8d18fd5a0cc9.png)


### Mobile layout

![image](https://user-images.githubusercontent.com/95375495/144472464-cb288c77-a09a-4f1b-acd8-92f67cfa6bc3.png)


![image](https://user-images.githubusercontent.com/95375495/144472720-bf2abeb8-456e-4d33-8baf-06d01a4443d3.png)







