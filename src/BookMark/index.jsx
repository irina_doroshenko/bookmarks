import React from "react";
import { connect } from "react-redux";
import { bookMarkDelete, bookMarkUpdate } from "../Actions/bookMarks.js";
import BookMarkInlineUpdatingForm from "../BookMarkForm/BookMarkInlineUpdatingForm";
import { update } from '../Validation/bookmarkvalidation'
import linkLogo from './arrow.svg'
import "./bookMark.css"

function BookMark({
    bookMarkDelete,
    bookMarkUpdate,
    item: {
        url,
        title,
        id,
        tags
    }, 
}) {

    function deleteHandler(e) {
        bookMarkDelete(id);
    }

    function updateHandler(item, tags) {
        bookMarkUpdate(id, item, tags);
    }

    function openUrlHandler(e) {
        window.open(url);
    }

    return (
        <div className='book-mark'>
            <div className='book-mark__content'>
                <BookMarkInlineUpdatingForm
                    buttonHandler={updateHandler}
                    defaultValue={{
                        urlAddres: url,
                        title: title,
                        tags: tags
                    }}
                    validationRules={update}
                />
            </div>
            <div
                onClick={deleteHandler}
                className="book-mark__delete"
            >
            </div>
            <div
                onClick={openUrlHandler}
                className="book-mark__update"
            >
                <img src={linkLogo} alt="" className="logo" />
            </div>
        </div>
    )
}

const mapDispatchToProps = {
    bookMarkDelete,
    bookMarkUpdate,
}

export default connect(null, mapDispatchToProps)(BookMark);