let Storage = {

    _createId() {
        return '_' + Math.random().toString(36).substr(2, 9);
    },
    _filterByTags(tags) {

        let items = this.getAllItems();

        if (!tags.length) return items;

        let filteredItems = items.filter((item) => {
            let filterResult;
            for (let i = 0; i < tags.length; i++) {
                filterResult = item.tags.find((tag) => tag === tags[i]) ? true : false;
                if (!filterResult) break;
            }
            return filterResult;
        })

        return filteredItems;
    },

    getAllItems() {
        return JSON.parse(localStorage.getItem("bookmarks")) || [];
    },

    getAllTags() {
        return JSON.parse(localStorage.getItem("tags")) || [];
    },

    getItemsByTag(...tags) {
        return this._filterByTags(tags);
    },

    createItem(item) {
        let id = this._createId();
        item.id = id;
        let storageResponse = this.getAllItems();
        storageResponse.push(item);
        localStorage.setItem("bookmarks", JSON.stringify(storageResponse));
        return item.id;
    },

    updateItem(id, item) {
        let storageResponse = this.getAllItems();
        let itemIndex = storageResponse.findIndex((element) => element.id === id);
        storageResponse[itemIndex] = item;
        storageResponse[itemIndex].id = id;

        localStorage.setItem("bookmarks", JSON.stringify(storageResponse));
    },

    createTags(...tags) {

        let storageResponse = this.getAllTags();

        //check tags existings
        for (let i = 0; i < tags.length; i++) {
            if (!storageResponse.find(element => element === tags[i].toLowerCase())) {
                storageResponse.push(tags[i].toLowerCase());
            }
        }

        localStorage.setItem("tags", JSON.stringify(storageResponse));


    },

    removeItem(itemId) {
        let storageResponse = this.getAllItems();
        storageResponse = storageResponse.filter(item => item.id !== itemId);
        localStorage.setItem("bookmarks", JSON.stringify(storageResponse));

    },
}

export default Storage;