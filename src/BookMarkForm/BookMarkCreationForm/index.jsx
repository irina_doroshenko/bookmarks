import InputText from "../../TextInput";
import TagsInput from "../../TagsInput";
import createBookMarkForm from "../BookMarkBaseForm";
import './BookMarkCreationForm.scss';

function BookMarkCreationForm({
    onFormValuesChange,
    formValues: {
        urlAddres,
        title,
        tags,
    },
    existingTags,
    onFormSubmit,
    formErrors,
}) {

    return (
        <form className={`Book-Mark-Form Book-Mark-Form_creation`}>
            <div className="Book-Mark-Form__title">create bookmark</div>
            <div className="Book-Mark-Form__element">
                <InputText
                    name="urlAddres"
                    value={urlAddres}
                    labelText="URL"
                    onChange={onFormValuesChange}
                />
                <div className="Book-Mark-Form_error">{formErrors.urlAddres || null}</div>
            </div>

            <div className="Book-Mark-Form__element">
                <InputText
                    name="title"
                    value={title}
                    labelText="title"
                    onChange={onFormValuesChange}
                />
                <div className="Book-Mark-Form_error">{formErrors.title || null}</div>
            </div>

            <div className="Book-Mark-Form__element">
                <TagsInput
                    name="tags"
                    tagList={tags}
                    existingsTags={existingTags}
                    labelText="tag(-s)"
                    onTagListChange={onFormValuesChange}
                    createEnabledFlag
                />
                <div className="Book-Mark-Form_error">{formErrors.tags || null}</div>
            </div>

            <div className="Book-Mark-Form_btn" onClick={onFormSubmit}>create</div>
        </form>
    );
}

export default createBookMarkForm(BookMarkCreationForm);