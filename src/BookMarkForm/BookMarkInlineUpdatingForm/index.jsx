import React, { useEffect, useState } from "react";
import TagsInput from "../../TagsInput";
import Tag from "../../Tag";
import whithTollTip from "../../ToolTip";
import editLogo from './edit.svg'
import doneLogo from './done.svg'
import './BookMarkInlineUpdatingForm.css'
import createBookMarkForm from "../BookMarkBaseForm";

function BookMarkInlineUpdatingForm({
    onFormValuesChange,
    formValues: {
        urlAddres,
        title,
        tags,
    },
    defaultValue,
    existingTags,
    onFormSubmit,
    formErrors,
}) {

    const mapiingFormValuesToInputNames = {
        titleInputName: 'title',
        urlInputName: 'urlAddres',
        tagsInputName: 'tags',
    }

    const [isEditingMode, setEditingMode] = useState({
        urlAddres: false,
        title: false,
        tags: false,
    });

    const [previousUpdatedInput, setPreviousUpdatedInput] = useState({ name: '' });

    useEffect(() => {
        if (!formErrors[previousUpdatedInput.name]) {
            setEditingMode(prevState => {
                return {
                    ...prevState,
                    [previousUpdatedInput.name]: false
                }
            })
        }

    }, [formErrors, previousUpdatedInput])

    function formValuesChangeHandler(e) {
        onFormValuesChange(e.target.name, e.target.value);
    }

    function changeFieldMode(inputName) {

        setEditingMode(prevState => {
            return {
                ...prevState,
                [inputName]: !prevState[inputName]
            }
        })
    }

    const getUrlHostName = (urlValue) => {
        let url = new URL(urlValue);
        return `${url.protocol}//${url.hostname}`;
    }

    const TitleWhithTollTip = whithTollTip((props) => (<div {...props}>{props.children}</div>), defaultValue.title);

    const titleReadingView = (
        <>
            {
                defaultValue.title.length >= 66 ?
                    <TitleWhithTollTip className={`element__title`}>
                        {defaultValue.title.substr(0, 43).concat('...')}
                    </TitleWhithTollTip> :
                    <div className={`element__title`}>
                        {defaultValue.title}
                    </div>
            }
            <div
                className="element__edit"
                onClick={(e) => {
                    changeFieldMode(mapiingFormValuesToInputNames.titleInputName);
                }}
            >
                <img src={editLogo} alt="" className="logo" />
            </div>
        </>
    );

    const titleUpdatingView = (
        <>
            <input
                className={`element__input`}
                type="text"
                name={mapiingFormValuesToInputNames.titleInputName}
                value={title}
                onChange={formValuesChangeHandler}
                autoComplete='false'
                autoFocus
                onKeyDown={(e) => {

                    if (e.key === 'Enter') {
                        e.preventDefault();
                        return false;

                    }
                }}
            />
            <div className="element_error">{formErrors.title || null}</div>

            <div
                className="element__edit"
                onClick={(e) => {

                    setPreviousUpdatedInput({ name: mapiingFormValuesToInputNames.titleInputName });
                    onFormSubmit(e);
                }
                }
            >
                <img src={doneLogo} alt="" className="logo" />
            </div>
        </>
    );

    const urlReadingView = (
        <>
            <div className="element__url">
                <a href={defaultValue.urlAddres} target="_blank" rel="noreferrer">{getUrlHostName(defaultValue.urlAddres)}</a>
            </div>
            <div
                className="element__edit"
                onClick={(e) => {
                    changeFieldMode(mapiingFormValuesToInputNames.urlInputName);
                }}
            >
                <img src={editLogo} alt="" className="logo" />
            </div>
        </>
    );

    const urlUpdatingView = (
        <>
            <input
                className="element__input"
                type="text"
                name={mapiingFormValuesToInputNames.urlInputName}
                value={urlAddres}
                onChange={formValuesChangeHandler}
                autoComplete='false'
                autoFocus
                onKeyDown={(e) => {

                    if (e.key === 'Enter') {
                        e.preventDefault();
                        return false;

                    }
                }}
            />
            <div className="element_error">{formErrors.urlAddres || null}</div>
            <div
                className="element__edit"
                onClick={(e) => {
                    setPreviousUpdatedInput({ name: mapiingFormValuesToInputNames.urlInputName });
                    onFormSubmit(e);
                }}
            >
                <img src={doneLogo} alt="" className="logo" />
            </div>

        </>
    );

    const tagsReadingView = (
        <>
            <div
                className="element__tags"
            >
                {
                    defaultValue.tags.map(tag => (
                        <Tag
                            tagName={tag}
                            classList={["element__tag"]}
                            key={tag}
                        />
                    ))
                }
            </div>
            <div
                className="element__edit"
                onClick={(e) => {
                    changeFieldMode(mapiingFormValuesToInputNames.tagsInputName);
                }}
            >
                <img src={editLogo} alt="" className="logo" />
            </div>
        </>
    );

    const tagsUpdatingView = (
        <>
            <div className='element__input'>
                <TagsInput
                    name={mapiingFormValuesToInputNames.tagsInputName}
                    tagList={tags}
                    existingsTags={existingTags}
                    labelText="tag(-s)"
                    onTagListChange={onFormValuesChange}
                    createEnabledFlag
                />
            </div>
            <div className="element_error">{formErrors.tags || null}</div>
            <div
                className="element__edit"
                onClick={(e) => {
                    setPreviousUpdatedInput({ name: mapiingFormValuesToInputNames.tagsInputName });
                    onFormSubmit(e);
                }}
            >
                <img src={doneLogo} alt="" className="logo" />
            </div>
        </>
    );


    return (
        <form
            className={`Book-Mark-Form Book-Mark-Form_inline-updation`}
        >
            <div className="Book-Mark-Form__element element">
                <div
                    className="form-element__wrapper"
                >
                    {!isEditingMode.title ? titleReadingView : titleUpdatingView}

                </div>
            </div>

            <div className="Book-Mark-Form__element Book-Mark-Form__element_shrink element">
                <div
                    className="form-element__wrapper"
                >
                    {!isEditingMode.urlAddres ? urlReadingView : urlUpdatingView}

                </div>
            </div>

            <div className="Book-Mark-Form__element">
                <div
                    className="form-element__wrapper"
                >
                    {!isEditingMode.tags ? tagsReadingView : tagsUpdatingView}

                </div>
            </div>

        </form>
    );
}

export default createBookMarkForm(BookMarkInlineUpdatingForm)