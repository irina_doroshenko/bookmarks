import React from "react";
import { connect } from "react-redux";
import './BookMarkBaseForm.scss'

function mapStateToProps(state) {
    return {
        existingTags: state.tagList,
    }
}

export default function createBookMarkForm (WrappedFormLayout) {

    return connect(mapStateToProps)(class extends React.Component { 

        constructor(props) {
            super(props);
            this.state = {
                formValues: this.getEmptyFormValues(),
                formErrors: {},
                submitingCount: 0,
            }
            this.validationRulesMapping = {
                urlAddres: props.validationRules.urlAddresValidation,
                title: props.validationRules.titleValidation,
                tags: props.validationRules.tagsValidation,
            }
        }
    
        componentDidMount() {

            // set defaultValue first time (if it exist)
            if (this.props.defaultValue) {
                this.setState({formValues: this.getDefaultFormValuesFromProps()});
            }
        }
    
        componentDidUpdate(prevProps, prevState) {

            // set new defaultValue after props updating (if default value was updated)
            if (this.isPropsDefaultValueUpdated(prevProps, prevState)) {
                this.setState({formValues: this.getDefaultFormValuesFromProps()});
            }
            
        }

        getEmptyFormValues = () => ({
            urlAddres: '',
            title: '',
            tags: [],
        })

        getDefaultFormValuesFromProps = () => ({
            urlAddres: this.props.defaultValue.urlAddres,
            title: this.props.defaultValue.title,
            tags: this.props.defaultValue.tags,
        })

        isPropsDefaultValueUpdated = (prevProps, prevState) => {
            if (this.props.defaultValue 
                && prevState.submitingCount !== this.state.submitingCount
            ) {
                return true;
            }
            return false;
        }

        isFormValuesUpdated = () => {
            if (this.props.defaultValue
                && JSON.stringify(this.state.formValues)
                === JSON.stringify(this.props.defaultValue)
                && Object.keys(this.state.formErrors).length === 0
            ) {
                return false;
            }
            return true;
        }



        changeFormValuesHandler = (name, value) => {
            this.setState((state) => ({
                formValues: {
                    ...state.formValues,
                    [name]: value
                },
            }));
        }
    
        formSubmitingHandler = (e) => {
            e.preventDefault();

            // check submiting necessity
            if (!this.isFormValuesUpdated()) return;

            // validate/submit form
            let validationErorrs = this.formValidate(this.state.formValues);
            
            if (Object.keys(validationErorrs).length === 0) {

                let item = {
                    url: this.state.formValues.urlAddres,
                    title: this.state.formValues.title,
                    tags: this.state.formValues.tags
                }
                
                this.props.buttonHandler(item, this.state.formValues.tags);
    
                this.setState((prevState) => (
                    {
                        formValues: this.getEmptyFormValues(),
                        formErrors: {},
                        submitingCount: prevState.submitingCount + 1,
                    }));

            } else {
                this.setState({
                    formErrors: validationErorrs,
                })
            }
        }
    
        formValidate = () => {
            let errors = {};
            for (let formValuesKey of Object.keys(this.state.formValues)) {
                if (this.validationRulesMapping[formValuesKey]) {
                    let validationErrorsArray =
                        this.validationRulesMapping[formValuesKey](this.state.formValues[formValuesKey]);
                    if (validationErrorsArray.length) {
                        errors[formValuesKey] = validationErrorsArray[0];
                    }
                }
            }
    
            return errors;
        }
    
        render() {
            return (
                <WrappedFormLayout
                    defaultValue={this.props.defaultValue || undefined}
                    onFormValuesChange={this.changeFormValuesHandler}
                    formValues={this.state.formValues}
                    existingTags={this.props.existingTags}
                    onFormSubmit={this.formSubmitingHandler}
                    formErrors={this.state.formErrors}
                />
            )
        }
    })
}