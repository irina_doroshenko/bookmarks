import Storage from "../storage/LocalStorage"

export function fetchTags() {
    return function(dispatch) {
        const tagList = Storage.getAllTags();
        dispatch({
            type: 'tags/tagsLoaded',
            payload: tagList,
        }); 
    }
}

export function tagsCreate(tags) {
    return function (dispatch) {
        Storage.createTags(...tags);
        const tagList = Storage.getAllTags();
        dispatch({
            type: 'tags/tagsChanged',
            payload: tagList,
        }); 
    }
}