import Storage from "../storage/LocalStorage"

export function fetchBookMarks() {
    return function(dispatch) {
        const bookMarkList = Storage.getAllItems();
        dispatch({
            type: 'bookMarks/bookMarksLoaded',
            payload: bookMarkList,
        });
    }
}

export function bookMarkCreate(item, tags) {
    return function (dispatch) {
        Storage.createTags(...tags);
        const tagList = Storage.getAllTags();
        dispatch({
            type: 'tags/tagsChanged',
            payload: tagList,
        });
        Storage.createItem(item);
        const bookMarkList = Storage.getAllItems();
        dispatch({
            type: 'bookMarks/bookMarksChanged',
            payload: bookMarkList,
        });
        dispatch ({
            type: 'filters/tagsFilterReset',
        })
    }
}

export function bookMarkDelete(itemId) {
    return function (dispatch, getState) {
        Storage.removeItem(itemId);
        let tagListFilterValue = getState().tagsFilter;
        const bookMarkList = Storage.getItemsByTag(...tagListFilterValue);
        dispatch({
            type: 'bookMarks/bookMarksChanged',
            payload: bookMarkList,
        }); 
        // const bookMarkList = Storage.getAllItems();
        // dispatch({
        //     type: 'bookMarks/bookMarksChanged',
        //     payload: bookMarkList,
        // });
    }
    
}

export function bookMarkUpdate(itemId, newItem, tags) {
    return function (dispatch, getState) {
        Storage.createTags(...tags);
        const tagList = Storage.getAllTags();
        dispatch({
            type: 'tags/tagsChanged',
            payload: tagList,
        });

        Storage.updateItem(itemId, newItem);

        let tagListFilterValue = getState().tagsFilter;
        const bookMarkList = Storage.getItemsByTag(...tagListFilterValue);
        dispatch({
            type: 'bookMarks/bookMarksChanged',
            payload: bookMarkList,
        }); 

        // const bookMarkList = Storage.getAllItems();
        // dispatch({
        //     type: 'bookMarks/bookMarksChanged',
        //     payload: bookMarkList,
        // }); 
    }
    
}

export function bookMarkFilterByTags() {
    return function (dispatch, getState) {
        let tagListFilterValue = getState().tagsFilter;
        // if (!tagListFilterValue.length) {
        //     const bookMarkList = Storage.getAllItems();
        //     dispatch({
        //         type: 'bookMarks/bookMarksChanged',
        //         payload: bookMarkList,
        //     }); 
        //     return;
        // }
        const bookMarkList = Storage.getItemsByTag(...tagListFilterValue);
        dispatch({
            type: 'bookMarks/bookMarksChanged',
            payload: bookMarkList,
        }); 

    }
}