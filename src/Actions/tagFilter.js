export function tagsFilterUpdate(tagList) {
    return function (dispatch) {
        dispatch({
            type: 'filters/tagsFilterChanged',
            payload: tagList,
        });
    }
}
