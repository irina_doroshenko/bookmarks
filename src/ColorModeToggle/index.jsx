import './colorModeToggle.css' 
import moonLogo from './moon-logo.svg'
import sunLogo from './sun-logo.svg'

export default function ColorModeToggle({
    onColorModeChange,
    colorMode,
}) {

  
    return (
        <div
            className={`color-mode-change color-mode-change_${colorMode}`}
            onClick={onColorModeChange}
        >
            <div className="color-mode-change__logo color-mode-change__logo_left">
                <img src={moonLogo} alt="" />
            </div>
            <div className="color-mode-change__logo color-mode-change__logo_right">
                <img src={sunLogo} alt="" />
            </div>
            <div 
                className={`color-mode-change__toggle color-mode-change__toggle_${colorMode}`}
            />

        </div>
    )
}