import Validation from "./rules";
const urlAddresRules = [Validation.requiredTextInput, Validation.urlValidation];
const titleRules = [Validation.requiredTextInput];
const tagsRules = [Validation.requiredObjectInput, Validation.maxObjectKeyLength(7)];

const createValidator = (rules) => {
    return (value) => {
        let errors = [];

        for (let i = 0; i < rules.length; i++) {
            let error = rules[i](value);
            if (error) {
                errors.push(error)
            }
        }

        return errors;
    }
}

export const create = {
    urlAddresValidation: createValidator(urlAddresRules),
    titleValidation: createValidator(titleRules),
    tagsValidation: createValidator(tagsRules),
}

export const update = {
    urlAddresValidation: createValidator(urlAddresRules),
    titleValidation: createValidator(titleRules),
    tagsValidation: createValidator(tagsRules)
}