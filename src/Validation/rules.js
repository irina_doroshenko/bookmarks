const Validation = {
    requiredTextInput: function (value) {
        return !value ? "field is required!" : null;
    },
    requiredObjectInput: function (value) {
        if (value.__proto__ === Array.prototype) {
            return !value.length ? "field is required!" : null;
        }

        if (value.__proto__ === Object.prototype) {
            return !Object.keys(value).length ? "field is required!" : null;
        }

    },

    maxObjectKeyLength: function (maxLen) {
        return function (value) {
            if (value.__proto__ === Array.prototype) {
                return value.length > maxLen ? `max ${maxLen} elements!` : null;
            }

            if (value.__proto__ === Object.prototype) {
                return Object.keys(value).length > maxLen ? `max ${maxLen} elements!` : null;
            }
        }
    },

    urlValidation: function(urlValue) {
        try {
            new URL(urlValue);
            return null;
          } catch (error) {
            return "url is invalid!";
          }
    }

}

export default Validation;