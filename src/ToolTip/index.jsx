import React from "react";
import './TollTip.css';

export default function whithTollTip(WrappedComponent, toolTipText) {

    return class extends React.Component {

        constructor(props) {
            super(props);
            this.state = {
                isToolTipOpen: false,
                mousePosition: {
                    x: undefined,
                    y: undefined,
                }
            }
        }

        showToolTipHandler = (e) => {
            this.setState({
                isToolTipOpen: true,
            })
        }

        hideToolTipHandler = (e) => {
            this.setState({
                isToolTipOpen: false,
            })
        }

        render() {

            return (
                <>
                    <div
                        className="tool-tip-wrapper"
                        onMouseOver={this.showToolTipHandler}
                        onMouseLeave={this.hideToolTipHandler}
                    >
                        <WrappedComponent {...this.props}>
                            {this.props.children}
                        </WrappedComponent>
                        {this.state.isToolTipOpen ? <div className={`tool-tip`}>{toolTipText}</div> : null}
                    </div>

                </>

            )
        }

    }
}