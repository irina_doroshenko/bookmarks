import React from "react";
import { connect } from "react-redux";
import BurgerSideBar from "./BurgerSideBar"
import BookMarkCreationForm from "./BookMarkForm/BookMarkCreationForm";
import BookMarkTable from "./BookMarkTable";
import BookMark from "./BookMark";
import { create } from './Validation/bookmarkvalidation'
import Wraper from "./Wraper";
import { bookMarkCreate, fetchBookMarks } from "./Actions/bookMarks.js";
import { fetchTags } from './Actions/tag.js';
import FilteringByTagsController from "./FilteringByTagsController";
import TagsInput from "./TagsInput";
import ColorModeToggle from "./ColorModeToggle";
import "./index.css";
import "./mode.scss"


class App extends React.Component {
    constructor(props) {
        super(props);

        this.appColorModes = {
            light: "light",
            dark: "dark",
        }

        this.state = {
            isBurgerSideBarOpen: false,
            isBodyEnable: true,
            colorMode: this.appColorModes.light,
        }
    }

    componentDidMount() {

        document.getElementById('root').classList.add("color-mode", `color-mode_${this.state.colorMode}`);
        this.props.fetchTags();
        this.props.fetchBookMarks();

    }

    componentDidUpdate(prevProps, prevState) {

        if (!this.state.isBodyEnable) {
            document.body.classList.add("body_blocked");
            document.getElementById('root').classList.add('root_blocked');
        } else {
            document.body.classList.remove("body_blocked");
            document.getElementById('root').classList.remove('root_blocked');
        }

        if (prevState.colorMode !== this.state.colorMode) {
            document.getElementById('root').classList.remove(`color-mode_${prevState.colorMode}`);
            document.getElementById('root').classList.add(`color-mode_${this.state.colorMode}`);
        }

    }

    onAppColorModeChange = (e) => {
        this.setState((prevState) => {
            let nextColorSate = prevState.colorMode === this.appColorModes.light ? this.appColorModes.dark : this.appColorModes.light;
            return {
                colorMode: nextColorSate,
            }
        })
    }

    onBurgerSideBarClick = () => {
        this.setState((prevState) => ({
            isBurgerSideBarOpen: !prevState.isBurgerSideBarOpen,
            isBodyEnable: !prevState.isBodyEnable
        }));
    }

    onItemCreate = (item, tags) => {
        this.props.bookMarkCreate(item, tags);
    };

    render() {
        return (
            <React.Fragment>
                <ColorModeToggle
                    colorMode={this.state.colorMode}
                    onColorModeChange={this.onAppColorModeChange}
                />
                {/* <div 
                    className={`color-mode-change color-mode-change_${this.state.colorMode}`}
                    onClick={this.onAppColorModeChange}
                /> */}
                <header className="header">
                    <Wraper wrapperModificators={["center", "big"]}>
                        <div className="header__container">
                            <FilteringByTagsController
                                itemContainerView={this.itemTableView}
                                tagsInputView={TagsInput}
                            />
                        </div>
                    </Wraper>
                </header>
                <main className="main">
                    
                    <BurgerSideBar
                        isOpen={this.state.isBurgerSideBarOpen}
                        onClick={this.onBurgerSideBarClick}
                    >
                        <BookMarkCreationForm
                            buttonHandler={this.onItemCreate}
                            validationRules={create}
                        />
                    </BurgerSideBar>
                    <div className="main_container">
                        <Wraper
                            wrapperModificators={["center", "big"]}
                        >
                            <BookMarkTable
                                itemView={BookMark}
                            />
                        </Wraper>
                    </div>
                </main>
            </React.Fragment >
        );
    }
}

const mapDispatchToProps = {
    bookMarkCreate,
    fetchTags,
    fetchBookMarks
}

export default connect(null, mapDispatchToProps)(App);