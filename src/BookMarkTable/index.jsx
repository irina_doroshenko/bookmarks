import React from "react";
import { useSelector } from "react-redux";
import "./bookMarkTable.css"

function ItemTable({ itemView: ItemView }) {
    let itemList = useSelector(state => state.bookMarkList);

    return (
        <div className="table-wrapper">
            <div className="bookMark-table">
                {
                    itemList.map(item => (
                        <div
                            className="element-wrapper"
                            key={item.id}
                        >
                            <div
                                className="bookMark-table__element"
                            >
                                {
                                    <ItemView
                                        item={item}
                                    />
                                }
                            </div>
                        </div>
                    ))}
            </div>
        </div>)
}

export default ItemTable