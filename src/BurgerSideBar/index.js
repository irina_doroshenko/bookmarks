import "./burgerSideBar.css"

function BurgerSideBar({
    isOpen,
    onClick,
    children,
}) {

    let sideBarClassName = !isOpen ? "BurgerSideBar_wrapper__close" : "BurgerSideBar_wrapper__open";
    let controlButtonClassName = isOpen ? "BurgerSideBar_btn__close" : "BurgerSideBar_btn__open";

    return (
        <div className={`BurgerSideBar_wrapper ${sideBarClassName}`}>
            <div className={`BurgerSideBar_btn ${controlButtonClassName}`} onClick={onClick}>
                <div className="line line__1 line__crossed"></div>
                <div className="line line__2 line__crossed"></div>
            </div>
            <div className="BurgerSideBar">
                <div className="BurgerSideBar_content-container">
                    {children}
                </div>

            </div>
        </div>
    )
}

export default BurgerSideBar;