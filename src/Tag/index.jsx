import React from "react";
import "./tag.css"

function Tag({
    onDelete,
    tagName,
    classList
}) {
    let onDeleteHandler = (e) => {
        if (onDelete) {
            onDelete(tagName);
        }
    }
    return (
        <div className={`tag ${classList.join(' ')}`}>
            {tagName}
            <div className="tag__btn" onClick={onDeleteHandler}></div>
        </div>
    );
}

export default Tag;