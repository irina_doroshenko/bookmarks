import React from "react";
import Tag from "../Tag";
import "./tagsInput.css";

class TagsInput extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
            suggestionTags: [],
            showSuggestions: false,
        };
    }

    changeInputHandler = (value) => {
        if (!value) {
            this.setState({
                inputValue: value,
                showSuggestions: false,
                suggestionTags: []
            });
            return;
        }
        let suggestionTags = this.props.existingsTags.filter(tag => tag.indexOf(value.toLowerCase()) !== -1);
        this.setState({
            inputValue: value,
            showSuggestions: true,
            suggestionTags: suggestionTags
        });
    }

    onEnterKeyDownHandler = (e) => {
        if (e.key === "Enter") {
            e.preventDefault();
            this.onTagAdd(this.state.inputValue);
        }
    }

    onTagDelete = (tag) => {
        let currentTagList = [...this.props.tagList];
        let index = currentTagList.findIndex((element) => element === tag);
        currentTagList.splice(index, 1);
        this.props.onTagListChange(this.props.name, currentTagList);
    }

    onTagAdd = (tag) => {
        let currentTagList = [...this.props.tagList];
        if (tag && currentTagList.findIndex((element) => element === tag.toLowerCase()) === -1) {
            currentTagList.push(tag.toLowerCase());
            this.props.onTagListChange(this.props.name, currentTagList);
        }
        this.setState({
            inputValue: '',
            showSuggestions: false,
            suggestionTags: []
        });
    }

    suggestionSection = () => {
        if (!this.state.inputValue) return null;

        if (this.state.suggestionTags.length) {
            return (this.state.suggestionTags.map((tag) => (
                <div onClick={() => this.onTagAdd(tag)} key={tag}> {tag} </div>
            )))
        }
        return this.props.createEnabledFlag ?
            (<div onClick={() => this.onTagAdd(this.state.inputValue)}> create "{this.state.inputValue}" </div>) :
            (<div> no matches whith "{this.state.inputValue}" </div>)
    }

    render() {
        return (
            <>
                <div className="Tags-input-label">
                    {this.props.labelText}
                </div>
                <div className="Tags-input-wrapper">
                    <div className={`Tags-input ${this.state.showSuggestions ? 'Tags-input_open' : ''}`}>
                        {
                            this.props.tagList.map((tag) => (
                                <Tag
                                    onDelete={this.onTagDelete}
                                    tagName={tag}
                                    classList={["Tags-input__tag"]}
                                    key={tag}
                                />
                            ))
                        }
                        <input
                            type="text"
                            autoFocus
                            className="Tags-input__field"
                            value={this.state.inputValue}
                            onChange={(e) => this.changeInputHandler(e.target.value)}
                            onKeyDown={this.onEnterKeyDownHandler} />
                    </div>
                    {this.state.showSuggestions ?
                        (<div className="Tags-input-suggestion">
                            {this.suggestionSection()}
                        </div>) :
                        null}
                </div>
            </>
        )
    }

}

export default TagsInput;