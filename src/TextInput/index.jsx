import React from "react";
import "./inputText.css"

function InputText({
    onChange,
    name,
    value,
    labelText,
}) {
    function changeHandler(e) {
        let value = e.target.value;
        onChange(name, value);
    }
    return (
        <>
            <label
                htmlFor={name}
                className="input-text__label">
                {labelText}
            </label>
            <input
                className="input-text__element"
                type="text"
                name={name}
                value={value}
                autoComplete="off"
                onChange={changeHandler}
            >
            </input>
        </>
    );
}

export default InputText;