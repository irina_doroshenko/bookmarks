import React from "react";
import "./wrapper.scss"

function Wrapper({wrapperModificators, children}) {

    const modificatorsToClassesMapping = {
        big: 'wrapper__big',
        sml: 'wrapper__sml',
        medium: 'wrapper_medium',
        center: 'wrapper__center',
    }

    const classString = wrapperModificators
        .map(modifier => modificatorsToClassesMapping[modifier])
        .join(' ');

     return (
         <div className={`wrapper ${classString}`}>
             {
                 children
             }
         </div>
     )
}

export default Wrapper;