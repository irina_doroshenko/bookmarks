import { createStore, applyMiddleware } from "redux";
import rootReducer from "./Reducers/rootReducer";
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension'

const storeEnhancer = composeWithDevTools(
    applyMiddleware(thunkMiddleware));

const store = createStore(rootReducer, storeEnhancer);

export default store;