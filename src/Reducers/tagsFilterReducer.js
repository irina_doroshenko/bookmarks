const initalState = [];

export default function  tagsFilterReducer(state = initalState, action) {
    switch (action.type){
        case 'filters/tagsFilterChanged': {
            return [...action.payload]
        }
        case 'filters/tagsFilterReset' : {
            return []

        }
        default: { 
            return state;
        }

    }
}