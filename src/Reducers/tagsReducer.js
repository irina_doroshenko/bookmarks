const initalState = [];

export default function  tagsReducer(state = initalState, action) {
    switch (action.type){
        case 'tags/tagsLoaded': {
            return [...action.payload]
        }
        case 'tags/tagsChanged' : {
            return [...action.payload];
        }
        default: { 
            return state;
        }

    }
}