import { combineReducers } from "redux";
import bookMarkReducer from "./bookMarksReducer";
import tagsReducer from "./tagsReducer";
import tagsFilterReducer from "./tagsFilterReducer";


const rootReducer = combineReducers({
    bookMarkList: bookMarkReducer,
    tagList: tagsReducer,
    tagsFilter: tagsFilterReducer,
});

export default rootReducer;