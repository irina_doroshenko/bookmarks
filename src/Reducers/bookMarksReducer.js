const initalState = [];

export default function bookMarkReducer(state = initalState, action) {
    switch (action.type) {
        case 'bookMarks/bookMarksLoaded': {
            return [...action.payload]
        }
        case 'bookMarks/bookMarksChanged': {
            return [...action.payload]
        }
        default: {
            return state;
        }
    }
}