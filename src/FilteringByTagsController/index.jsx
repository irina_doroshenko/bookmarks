import React from "react";
import { connect } from "react-redux";
import { bookMarkFilterByTags } from "../Actions/bookMarks.js";
import {tagsFilterUpdate} from "../Actions/tagFilter"
import "./filteringByTagsController.scss"


class FilteringByTagsController extends React.Component {

    
    onTagListChange = (name, tags) => {
        // this.props.tagsFilterUpdate(tags);
        this.props.updateFilter(tags);
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (JSON.stringify(this.props.tagList) !== JSON.stringify(prevProps.tagList)) {
            this.props.filterBookmark(this.props.tagList);
            // this.props.getItemsByTag(this.props.tagList);
        }
    }

    render() {
        let { tagsInputView: TagsInputView } = this.props;

        return (
            <>
                <div className="filtering-by-tags-controller">
                    < TagsInputView
                        name="tagList"
                        tagList={this.props.tagList}
                        labelText="search by tags"
                        onTagListChange={this.onTagListChange}
                        existingsTags={this.props.existingTags}
                    />
                </div>
            </>
        );
    }

}

function mapStateToProps(state) {
    return {
        existingTags: state.tagList,
        tagList: state.tagsFilter,
    }
}

const mapDispatchToProps = {
   filterBookmark: bookMarkFilterByTags,
   updateFilter: tagsFilterUpdate,

}
export default connect(mapStateToProps, mapDispatchToProps)(FilteringByTagsController);
